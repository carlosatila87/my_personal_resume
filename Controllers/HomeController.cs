﻿using System.Web.Mvc;
using Umbraco.Web.Mvc;
using MyResumeSite.Models;
using System.Collections.Generic;
using Umbraco.Web;
using Umbraco.Core.Models;
using System.Linq;
using Archetype.Models;
using Newtonsoft.Json;
using System;
using Umbraco.Core;

namespace MyResumeSite.Controllers
{
    public class HomeController : SurfaceController
    {
        private const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/Home/";

        public ActionResult RenderCover()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_Cover.cshtml");
        }

        public ActionResult RenderAbout()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_About.cshtml");
        }

        public ActionResult RenderResume()
        {
            List<Timeline> timelineList = new List<Timeline>();
            IPublishedContent homePage = CurrentPage.AncestorOrSelf(1).DescendantsOrSelf().Where(x => x.DocumentTypeAlias == "home").FirstOrDefault();
            ArchetypeModel timelineCategories = homePage.GetPropertyValue<ArchetypeModel>("timeline");
            foreach(ArchetypeFieldsetModel fieldset in timelineCategories)
            {
                //CATEGORY FROM TIMELINE
                string category = fieldset.GetValue<string>("timelineCategory");
                if (!String.IsNullOrEmpty(category))
                {                    
                    //var timelineItems = JsonConvert.DeserializeObject<ArchetypeModel>(fieldset.GetValue<string>("timelineItems"));
                    var timelineItems = JsonConvert.DeserializeObject<ArchetypeModel>(fieldset.Properties.Where(x => x.Alias == "timelineItems").FirstOrDefault().Value.ToString());                    
                    List<TimelineItem> listItems = new List<TimelineItem>();
                    foreach (ArchetypeFieldsetModel itemFieldset in timelineItems)
                    {
                        string itemHeader = itemFieldset.GetValue<string>("timelineItemHeader");
                        string itemSubHeader = itemFieldset.GetValue<string>("timelineItemSubHeader");
                        string itemContent = itemFieldset.GetValue<string>("timelineItemContent");
                        string itemIcon = itemFieldset.GetValue<string>("timelineItemIcon");
                        List<ModalContent> itemModals = new List<ModalContent>();
                        if (!string.IsNullOrEmpty(itemFieldset.GetValue<string>("timelineItemModals")))
                        {
                            var timelineItemModals = JsonConvert.DeserializeObject<ArchetypeModel>(itemFieldset.GetValue<string>("timelineItemModals"));
                            //ArchetypeModel timelineItemModals = itemFieldSet.GetValue<ArchetypeModel>("timelineItemModals");                        
                            foreach (ArchetypeFieldsetModel itemModalFieldSet in timelineItemModals)
                            {
                                string modalTitle = itemModalFieldSet.GetValue<string>("modalTitle");
                                string modalContent = itemModalFieldSet.GetValue<string>("modalContent");
                                itemModals.Add(new ModalContent(modalTitle, modalContent));
                            }
                        }
                        listItems.Add(new TimelineItem(itemHeader, itemSubHeader, itemContent, itemIcon, itemModals));
                    }
                    timelineList.Add(new Timeline(category, listItems));
                }
            }
            string resumeTitle = Umbraco.AssignedContentItem.GetPropertyValue("resumeTitle").ToString();
            return PartialView(PARTIAL_VIEW_FOLDER + "_Resume.cshtml",new Tuple<List<Timeline>, string>(timelineList,resumeTitle));
        }

        public ActionResult RenderSkills()
        {
            List<SkillCollection> model = new List<SkillCollection>();
            IPublishedContent homePage = CurrentPage.AncestorOrSelf(1).DescendantsOrSelf().Where(x => x.DocumentTypeAlias == "home").FirstOrDefault();
            ArchetypeModel skills = homePage.GetPropertyValue<ArchetypeModel>("skillsCollection");
            foreach (ArchetypeFieldsetModel fieldset in skills)
            {
                string category = fieldset.GetValue<string>("skillCategory");
                if (!String.IsNullOrEmpty(category))
                {                    
                    var skillItems = JsonConvert.DeserializeObject<ArchetypeModel>(fieldset.Properties.Where(x => x.Alias == "skillItems").FirstOrDefault().Value.ToString());
                    List<SkillItem> listItems = new List<SkillItem>();
                    foreach (ArchetypeFieldsetModel itemFieldset in skillItems)
                    {
                        string itemTitle = itemFieldset.GetValue<string>("skillTitle");
                        int itemRange = Convert.ToInt32(Umbraco.GetPreValueAsString(itemFieldset.GetValue<int>("skillRange")));                        
                        listItems.Add(new SkillItem(itemTitle,itemRange));
                    }
                    model.Add(new SkillCollection(category, listItems));
                }
            }
            string skillsTitle = Umbraco.AssignedContentItem.GetPropertyValue("skillsTitle").ToString();
            string extraSkills = Umbraco.AssignedContentItem.GetPropertyValue("extraSkills").ToString();
            List<string> parmSkills = new List<string>();
            parmSkills.Add(skillsTitle);
            parmSkills.Add(extraSkills);
            return PartialView(PARTIAL_VIEW_FOLDER + "_Skills.cshtml",new Tuple<List<SkillCollection>,List<string>>(model, parmSkills));
        }

        public ActionResult RenderPersonalAchievements()
        {
            List<ImageLibrary> model = new List<ImageLibrary>();
            IPublishedContent homePage = CurrentPage.AncestorOrSelf(1).DescendantsOrSelf().Where(x => x.DocumentTypeAlias == "home").FirstOrDefault();
            ArchetypeModel imageLibrary = homePage.GetPropertyValue<ArchetypeModel>("personalAchievementImages");
            foreach (ArchetypeFieldsetModel fieldset in imageLibrary)
            {
                string imageFullGuid = fieldset.Properties.Where(x => x.Alias == "imageLibraryImage").FirstOrDefault().Value.ToString();
                string imageFullUrl = "";
                if (!String.IsNullOrEmpty(imageFullGuid))
                {
                    Udi imageFullUdi = Udi.Parse(imageFullGuid);
                    int imageFullId = Umbraco.GetIdForUdi(imageFullUdi);
                    var imageFull = Umbraco.Media(imageFullId);
                    imageFullUrl = imageFull.Url;
                }
                string imageThumbGuid = fieldset.Properties.Where(x => x.Alias == "imageLibraryThumb").FirstOrDefault().Value.ToString();
                string imageThumbUrl = "";
                if (!String.IsNullOrEmpty(imageThumbGuid))
                {
                    Udi imageThumbUdi = Udi.Parse(imageThumbGuid);
                    int imageThumbId = Umbraco.GetIdForUdi(imageThumbUdi);
                    var imageThumb = Umbraco.Media(imageThumbId);
                    imageThumbUrl = imageThumb.Url;
                }
                string imageDescription = fieldset.GetValue<string>("imageLibraryDescription");
                string imageTitle = fieldset.GetValue<string>("imageLibraryTitle");
                model.Add(new ImageLibrary(imageTitle,imageDescription,imageThumbUrl, imageFullUrl));
            }
            string achTitle = Umbraco.AssignedContentItem.GetPropertyValue("personalAchievementsTitle").ToString();
            return PartialView(PARTIAL_VIEW_FOLDER + "_PersonalAchievements.cshtml",new Tuple<List<ImageLibrary>,string>(model,achTitle));
        }

        public ActionResult RenderEntrepreneurialExperiences()
        {
            List<ImageShow> model = new List<ImageShow>();
            IPublishedContent homePage = CurrentPage.AncestorOrSelf(1).DescendantsOrSelf().Where(x => x.DocumentTypeAlias == "home").FirstOrDefault();
            ArchetypeModel imageShowCollection = homePage.GetPropertyValue<ArchetypeModel>("xpImageCollection");
            foreach (ArchetypeFieldsetModel fieldset in imageShowCollection)
            {
                string imageGuid = fieldset.Properties.Where(x => x.Alias == "image").FirstOrDefault().Value.ToString();
                Udi imageUdi = Udi.Parse(imageGuid);                
                int imageId = Umbraco.GetIdForUdi(imageUdi);
                var image = Umbraco.Media(imageId);
                string imageUrl = image.Url;
                string imageDescription = fieldset.GetValue<string>("imageDescription");
                model.Add(new ImageShow(imageUrl, imageDescription));
            }
            string xpTitle = Umbraco.AssignedContentItem.GetPropertyValue("xpTitle").ToString();
            return PartialView(PARTIAL_VIEW_FOLDER + "_EntrepreneurialExperiences.cshtml",new Tuple<List<ImageShow>,string>(model,xpTitle));
        }

        public ActionResult RenderContact()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_Contact.cshtml");
        }
    }
}