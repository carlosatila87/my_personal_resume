﻿using MyResumeSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace MyResumeSite.Controllers
{
    public class SiteLayoutController : SurfaceController
    {
        private const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/SiteLayout/";

        public ActionResult RenderNavigation()
        {
            List<NavigationListItem> nav = GetNavigationModelFromDatabase();
            string homeTitle = Umbraco.AssignedContentItem.GetPropertyValue("homeTitle").ToString();
            return PartialView(PARTIAL_VIEW_FOLDER + "_Navigation.cshtml", new Tuple<List<NavigationListItem>,string>(nav,homeTitle) );
        }

        /// <summary>
        /// Finds the home page and gets the navigation structure based on it and it's children
        /// </summary>
        /// <returns>A List of NavigationListItems, representing the structure of the site.</returns>
        private List<NavigationListItem> GetNavigationModelFromDatabase()
        {            
            IPublishedContent homePage = CurrentPage.AncestorOrSelf(1).DescendantsOrSelf().Where(x => x.DocumentTypeAlias == "home").FirstOrDefault();
            List<NavigationListItem> nav = new List<NavigationListItem>();
            //nav.Add(new NavigationListItem(new NavigationLink(homePage.Url, homePage.Name))); //COMMENTED TO DONT INCLUDE HOME ON NAVIGATION
            nav.AddRange(GetChildNavigationList(homePage));
            return nav;
        }

        /// <summary>
        /// Loops through the child pages of a given page and their children to get the structure of the site.
        /// </summary>
        /// <param name="page">The parent page which you want the child structure for</param>
        /// <returns>A List of NavigationListItems, representing the structure of the pages below a page.</returns>
        private List<NavigationListItem> GetChildNavigationList(IPublishedContent page)
        {
            List<NavigationListItem> listItems = null;
            var childPages = page.Children.Where("Visible");
            if (childPages != null && childPages.Any() && childPages.Count() > 0)
            {
                listItems = new List<NavigationListItem>();
                foreach (var childPage in childPages)
                {
                    NavigationListItem listItem;
                    if (childPage.HasProperty("linkUrl"))
                    {
                        listItem = new NavigationListItem(new NavigationLink(childPage.GetPropertyValue("linkUrl").ToString(), childPage.GetPropertyValue("text").ToString(), Convert.ToBoolean(childPage.GetPropertyValue("newWindow")), childPage.GetPropertyValue("title").ToString()));
                    }
                    else
                    {
                        listItem = new NavigationListItem(new NavigationLink(childPage.Url, childPage.Name));
                    }                    
                    listItem.Items = GetChildNavigationList(childPage);
                    listItems.Add(listItem);
                }
            }
            return listItems;
        }

        public ActionResult RenderBottom()
        {            
            return PartialView(PARTIAL_VIEW_FOLDER + "_Bottom.cshtml");
        }
    }
}