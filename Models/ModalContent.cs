﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyResumeSite.Models
{
    public class ModalContent
    {
        public string Title { get; set; }
        public string Content { get; set; }

        public ModalContent(string title, string content)
        {
            Title = title;
            Content = content;
        }
    }
}