﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyResumeSite.Models
{
    public class TimelineItem
    {
        public string Header { get; set; }
        public string SubHeader { get; set; }
        public string Content { get; set; }
        public string Icon { get; set; }
        public List<ModalContent> Modals { get; set; }

        public TimelineItem(string header, string subHeader, string content, string icon, List<ModalContent> modals)
        {
            Header = header;
            SubHeader = subHeader;
            Content = content;
            Icon = icon;
            Modals = modals;
        }
    }
}