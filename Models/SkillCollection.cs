﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyResumeSite.Models
{
    public class SkillCollection
    {
        public string Category { get; set; }
        public List<SkillItem> Items { get; set; }

        public SkillCollection(string category, List<SkillItem> items)
        {
            Category = category;
            Items = items;
        }
    }
}