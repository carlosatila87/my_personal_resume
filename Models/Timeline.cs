﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyResumeSite.Models
{
    public class Timeline
    {
        public string Category { get; set; }
        public List<TimelineItem> Items { get; set; }

        public Timeline(string category, List<TimelineItem> items)
        {
            Category = category;
            Items = items;
        }
    }    
}