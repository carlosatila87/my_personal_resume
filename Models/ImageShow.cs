﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyResumeSite.Models
{
    public class ImageShow
    {
        public string ImageUrl { get; set; }
        public string ImageDescription { get; set; }

        public ImageShow(string imageUrl, string imageDescription)
        {
            ImageUrl = imageUrl;
            ImageDescription = imageDescription;
        }
    }
}