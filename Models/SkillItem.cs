﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyResumeSite.Models
{
    public class SkillItem
    {
        public string Title { get; set; }
        public int Range { get; set; }

        public SkillItem(string skillTitle, int skillRange)
        {
            Title = skillTitle;
            Range = skillRange;
        }
    }
}