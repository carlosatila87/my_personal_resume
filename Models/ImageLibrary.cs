﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyResumeSite.Models
{
    public class ImageLibrary
    {
        public string ImageTitle { get; set; }
        public string ImageDescription { get; set; }
        public string ImageThumbUrl { get; set; }
        public string ImageFullUrl { get; set; }

        public ImageLibrary (string imageTitle, string imageDescription, string imageThumb, string imageFull)
        {
            ImageTitle = imageTitle;
            ImageDescription = imageDescription;
            ImageThumbUrl = imageThumb;
            ImageFullUrl = imageFull; 
        }
    }
}